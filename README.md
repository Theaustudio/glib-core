# Documentation

![](https://gunivers.net/wp-content/uploads/2020/08/Glibs_banner.jpg)

The Gunivers-libs is a series of tool datapacks, that group together all the functions and systems that can be used in your maps, to simplify your task. The central element of which is the "Glib-Core" datapack. They allow to abstract some complex algorithms, i.e. to hide their complexity by simply providing the procedure to follow to use them. This abstraction also allows you not to have to worry about updating these functions, you just have to update the whole datapack in your map, and it will then use the most recent functions, without having to change anything in your code.

Thus, you will find fundamental functions such as trigonometry functions (sin, cos, tan ...) as well as practical functions such as :\
\- an ID system for entities\
\- a block <-> score conversion system to detect which block is targeted\
\- a pathfinding system\
\- or a system allowing the management of vectors to give very customized trajectories to your entities, which can be deflected by the wind, bounce on blocks etc ...

Because a short video is better than a long speech: https://www.youtube.com/watch?v=E2nKYEvjETk

* Main article : https://gunivers.net/gunivers-lib/
* Note : In order to save time, this traduction was mostly made with deepl.com and may contain mistakes. If you find one of these mistakes, you can report it to us on "our Discord": https://discord.gg/E8qq6tN

-> [Start explore](SUMMARY.md)

# Credits

## Concept
@LeiRoF

## Project Managers
@theogiraudet/Oromis, @LeiRoF

## Contributors
Dev: @theogiraudet/Oromis, @LeiRoF, @Luludatra, @KubbyDev, @.𝖎𝖐𝖇𝖗𝖚𝖓𝖊𝖑, @A~Z, @Redcoal, @Syl2010, @Kikipunk, @Loumardes 